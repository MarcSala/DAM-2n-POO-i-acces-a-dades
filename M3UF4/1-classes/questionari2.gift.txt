::Herència::[markdown]
Donat el següent diagrama de classes:
\n![Diagrama herència](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/docs/utilitzacio_avancada_de_classes/imatges/questionari2.png)
\nQuè passarà si inclouem el següent codi al programa?
\n    ClasseBase objecte \= new ClasseDerivada();
\n    objecte.metode();
{
  =El compilador donarà un error a la segona línia.
  ~El programa compilarà i s'executarà sense errors.
  ~El programa llançarà una excepció en temps d'execució.
  ~El compilador donarà un error a la primera línia.
}

::Mètodes per defecte::[markdown]
Donat el següent diagrama de classes:
\n![Diagrama mètodes per defecte](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/docs/utilitzacio_avancada_de_classes/imatges/questionari2_001.png)
\nLa interfície *Interficie* declara el mètode *metode* i li proporciona un codi per defecte. La classe *ClasseBase* implementa el mètode *metode*.
\nQuè s'executarà donat el següent segment de programa?
\n    ClasseDerivada objecte \= new ClasseDerivada();
\n    objecte.metode();
{
  =El codi de *metode* de *ClasseBase*.
  ~El codi de *metode* de *Interficie*.
  ~El compilador donarà un error perquè no pot determinar quin mètode és el correcte.
  ~Hi haurà un error en temps d'execució perquè no es podrà determinar quin mètode és el correcte.
}

::instanceof::[markdown]
Donat el següent diagrama de classes:
\n![Diagrama instanceof](https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades/raw/master/docs/utilitzacio_avancada_de_classes/imatges/questionari.png)
\nSi creem un objecte amb la sentència *Volador v = new Oreneta();*
\nQuantes de les següents expressions retornaran *true*?
\n    v instanceof Oreneta
\n    v instanceof Ocell
\n    v instanceof Animal
\n    v instanceof Volador
{
  =Totes.
  ~Només una.
  ~Només dues.
  ~Totes menys una.
}

::Herència de constructors::[markdown]
Quina serà la sortida del següent programa?\n
\n    public class Vehicle \{
\n	      public Vehicle() \{
\n		        System.out.print("Constructor Vehicle sense paràmetres. ");
\n	      \}
\n        public Vehicle(String matricula) \{
\n            System.out.print("Constructor Vehicle amb un paràmetre. ");
\n        \}
\n    \}
\n    public class Cotxe extends Vehicle \{
\n	      public Cotxe(String matricula) \{
\n		        System.out.print("Constructor cotxe. ");
\n	      \}
\n    \}
\n    public class Programa \{
\n	      public static void main(String args[]) \{
\n		        Vehicle v \= new Cotxe("5555AAA");
\n	      \}
\n    \}\n
{
  =Constructor Vehicle sense paràmetres. Constructor cotxe.
  ~Constructor cotxe. Constructor Vehicle sense paràmetres.
  ~Constructor cotxe. Constructor Vehicle amb un paràmetre.
  ~Constructor Vehicle amb un paràmetre. Constructor cotxe.
}

::final::[markdown]
Quina d'aquestes afirmacions **NO** és certa?
{
  =Un atribut *final* té el mateix valor per a tots els objectes de la classe.
  ~No es pot crear una classe que derivi d'una classe *final*.
  ~No es pot sobreescriure un mètode *final*.
  ~Un atribut *final* no es pot modificar un cop inicialitzat.
}

::abstract::[markdown]
Quina d'aquestes afirmacions **NO** és certa?
{
  =Una classe *abstract* ha de tenir algun mètode *abstract*.
  ~No es pot instanciar un objecte d'una classe *abstract*.
  ~Una classe que té mètodes *abstract* ha de ser *abstract*.
  ~Una classe *abstract* pot derivar d'una classe que no és *abstract*.
}

::Referències i tipus::[markdown]
Si *Ambulancia* és una classe derivada de *Cotxe*, cap de les dues abstractes, quina de les següents sentències és **incorrecta**?
{
  =Ambulancia a \= new Cotxe();
  ~Cotxe c \= new Ambulancia();
  ~Cotxe c \= new Cotxe();
  ~Ambulancia a \= new Ambulancia();
}

::Herència múltiple::[markdown]
Una classe diferent d'*Object*...
{
  =deriva sempre d'una única classe i pot implementar 0 o més interfícies.
  ~pot derivar de 0 o 1 classes i pot implementar 1 o més interfícies.
  ~pot derivar de 1 o més classes i pot implementar 0 o 1 interfícies.
  ~deriva sempre d'una única classe i pot implementar 0 o 1 interfícies.
}

::Comparable i Comparator::[markdown]
La interfície *Comparator*...
{
  =permet crear molts comparadors diferents per a la mateixa classe, cadascun dels quals pot comparar dos objectes de formes diferents.
  ~permet definir la forma natural com s'han de comparar dos objectes d'una classe i que s'utilitzarà per defecte en mètodes com *Array.sort()*.
  ~permet crear un comparador addicional al que proporciona la interfície *Comparable*, perquè una classe només pot implementar *Comparator* un cop.
  ~és un exemple d'una interfície sense cap mètode.
}

::Cloneable::[markdown]
El mètode *clone*...
{
  =és un mètode *protected* de la classe *Object*. La interfície *Cloneable* no té cap mètode.
  ~és un mètode declarat a la interfície *Cloneable* amb un codi per defecte que copia tots els atributs d'un objecte a un objecte nou del mateix tipus.
  ~és un mètode de la classe *Object* que cal sobreescriure si la nostra classe implementa *Cloneable* i tenim atributs que no siguin primitius o inmutables.
  ~és un mètode de la classe *Object* i també de la interfície *Cloneable* que cal sobreescriure si la nostra classe implementa aquesta interfície.
}

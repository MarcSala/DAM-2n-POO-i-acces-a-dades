package consultes_parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/*
 * 1. Fes un programa que demani a l'usuari 
 * el cognom d'un actor i que mostri per pantalla totes les seves pel·lícules.
 */
public class Ex1 {

	public void inicia() {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		String sql = "SELECT first_name, last_name, title FROM film"
				+" JOIN film_actor USING(film_id)" 
				+" JOIN actor USING(actor_id)" 
				+" WHERE last_name LIKE ? ORDER BY first_name, title";
		String cognomActor;
		Scanner entrada = new Scanner(System.in);
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				PreparedStatement statement = connexio.prepareStatement(sql)) {
			System.out.println("Introdueix el cognom d'un actor a cercar: ");
			cognomActor = entrada.nextLine().toUpperCase();
			statement.setString(1, cognomActor);
			try (ResultSet rs = statement.executeQuery();) {
				if(rs.isBeforeFirst()) {//Significa que sí que hi ha registres
					String nomActor;
					String nomActorAnterior = "";
					while (rs.next()){
						nomActor = rs.getString(1)+" "+rs.getString(2);
						System.out.print((nomActor.equals(nomActorAnterior)?"":"\n\nActor: "+nomActor+"\n")+"\n Pel·lícula: "+rs.getString(3));
						nomActorAnterior = nomActor;
					}
				} else {
					System.out.println("Cognom inexistent a la base de dades.\n");
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		entrada.close();
	}
	
	public static void main(String[] args) {
		(new Ex1()).inicia();
	}

}

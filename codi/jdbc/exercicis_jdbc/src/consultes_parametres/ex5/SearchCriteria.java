package consultes_parametres.ex5;

public enum SearchCriteria {
	TITLE("Cerca per títol"),
	ACTOR("Cerca per actor/actriu"),
	GENRE("Cerca per gènere");
	
	public final String info;
	
	private SearchCriteria(String info) {
		this.info = info;
	}
}

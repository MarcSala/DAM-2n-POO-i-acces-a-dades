package consultes_parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/*
2- Modifica l'exercici 3 per demanar a l'usuari el nom i cognom del client que
s'ha de mostrar.
 */
public class Ex2 {
	private static final String url = "jdbc:mysql://localhost:3306/sakila";
	private static final String user = "root";
	private static final String pass = "usbw";
	
	public static void main(String[] args) {
		String nomClient = "";
		String cognomClient = "";
		ResultSet rs = null;
		boolean hiHaResultats = false;
		String sql = "SELECT f.title, a.address, adddate(r.rental_date, f.rental_duration)" +
				" FROM film f" +
				" JOIN inventory i USING(film_id)" +
				" JOIN rental r USING (inventory_id)" +
				" JOIN customer c USING(customer_id)" +
				" JOIN store s ON i.store_id=s.store_id" +
				" JOIN address a ON a.address_id=s.address_id" +
				" WHERE r.return_date IS NULL" +
				" AND c.first_name LIKE ?" +
				" AND c.last_name LIKE ?";
		Scanner entrada = new Scanner(System.in);
		try (Connection connexio = DriverManager.getConnection(url,user,pass);
				PreparedStatement statement = connexio.prepareStatement(sql)) {
			
			System.out.print("Escriu el nom d'un client per buscar totes els items en lloger: ");
			nomClient = entrada.nextLine();
			System.out.print("Escriu el cognom del client: ");
			cognomClient = entrada.nextLine();
			
			statement.setString(1, nomClient);
			statement.setString(2, cognomClient);
			rs = statement.executeQuery();
			while(rs.next()) {
				if(!hiHaResultats) {
					hiHaResultats = true;
					System.out.println("Pel·lícules que té en lloger el client " + nomClient + cognomClient + "\n-----------------------------------------------------------------");
				}
				System.out.println("Títol pel·lícula: " + rs.getString(1) + "\t\tAdreça de la tenda: " + rs.getString(2) + "\t\tData Retorn: " + rs.getString(3));
			}
			if(!hiHaResultats)
				System.out.println("No s'ha trobat cap client amb aquest nom i cognom: " + nomClient + " " + cognomClient);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		entrada.close();
	}
}

package mongodb.exercicis.aggregation;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation1 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("students");

		/*
		 db.students.aggregate([
		 	{$unwind:"$scores"},
		 	{$match:{"scores.type":"exam"}},
		 	{$group:{
		 		_id:"$name",
		 		nota:{$avg:"$scores.score"}}},
		 	{$match:{nota:{$gte:40}}}
		 ])
		 */
		coll.aggregate(Arrays.asList(
			unwind("$scores"),
			match(eq("scores.type","exam")),
			group("$name", avg("nota", "$scores.score")),
			match(gte("nota", 40))
		)).forEach((Document doc) -> {
			System.out.println(doc.getString("_id")+" ("+doc.getDouble("nota")+")");
		});
		client.close();
	}

}

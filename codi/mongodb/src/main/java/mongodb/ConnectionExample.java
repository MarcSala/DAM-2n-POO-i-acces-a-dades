package mongodb;

import java.util.Arrays;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCursor;

public class ConnectionExample {

	public static void main(String[] args) {
		String user = "test";
		String database = "examples";
		char[] password = "password".toCharArray();

		MongoCredential credential = MongoCredential.
				createCredential(user, database, password);
		ServerAddress serverAddress = new ServerAddress("localhost");
		MongoClientOptions options = MongoClientOptions.builder()
				.maxWaitTime(4)
				.connectTimeout(4)
				.socketTimeout(4).build();

		try (MongoClient client = 
				new MongoClient(serverAddress, Arrays.asList(credential), options)) {
			MongoCursor<String> cursor = client.listDatabaseNames().iterator();
			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
		} catch (MongoException e) {
			System.err.println("MongoDB exception: "+e.getMessage());
		}
	}
}

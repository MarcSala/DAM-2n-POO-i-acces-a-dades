## Exercicis

### Format JSON

#### Exercici 1

Tradueix el següent fragment XML (extret de
http://www.w3schools.com/xml/cd_catalog.xml) a JSON.

```xml
<CATALOG>
  <CD>
    <TITLE>Empire Burlesque</TITLE>
    <ARTIST>Bob Dylan</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Hide your heart</TITLE>
    <ARTIST>Bonnie Tyler</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS Records</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
</CATALOG>
```

#### Exercici 2

Tradueix el següent fragment XML (extret de http://json.org/example.html) a
JSON.

```xml
<menu id="file" value="File">
  <popup>
    <menuitem value="New" onclick="CreateNewDoc()" />
    <menuitem value="Open" onclick="OpenDoc()" />
    <menuitem value="Close" onclick="CloseDoc()" />
  </popup>
</menu>
```

#### Exercici 3

Tradueix el següent fragment JSON (extret de http://json.org/example.html)
a XML.

```json
{
  "glossary": {
    "title": "example glossary",
		"GlossDiv": {
      "title": "S",
			"GlossList": {
        "GlossEntry": {
          "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
            "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
          },
					"GlossSee": "markup"
        }
      }
    }
  }
}
```

[Solucions](exercicis/solucions_format_json.md)

### Representació de documents

#### Exercici 1

Utilitza les classes proporcionades pel connector de MongoDB per crear un
programa en Java que creï el següent document JSON.

Utilitza un bucle per afegir els actors i no repetir codi.

```json
{
	"_id" : 19,
	"Title" : "AMADEUS HOLY",
	"Description" : "A Emotional Display of a Pioneer And a Technical Writer who must Battle a Man in A Baloon",
	"Length" : 113,
	"Rating" : "PG",
	"Special Features" : "Commentaries,Deleted Scenes,Behind the Scenes",
	"Rental Duration" : 6,
	"Replacement Cost" : 20.99,
	"Category" : "Action",
	"Actors" : [
		{
			"actorId" : 5,
			"Last name" : "LOLLOBRIGIDA",
			"First name" : "JOHNNY"
		},
		{
			"actorId" : 27,
			"Last name" : "MCQUEEN",
			"First name" : "JULIA"
		},
		{
			"actorId" : 37,
			"Last name" : "BOLGER",
			"First name" : "VAL"
		},
		{
			"actorId" : 43,
			"Last name" : "JOVOVICH",
			"First name" : "KIRK"
		},
		{
			"actorId" : 84,
			"Last name" : "PITT",
			"First name" : "JAMES"
		},
		{
			"actorId" : 104,
			"Last name" : "CRONYN",
			"First name" : "PENELOPE"
		}
	]
}

```

#### Exercici 2

Fes el mateix amb el següent document.

Pots considerar que les dates es guarden en format de cadena.

Utilitza un bucle per no repetir codi a l'hora d'afegir els *Rentals*.

```json
{
	"_id" : 1,
	"First Name" : "MARY",
	"Last Name" : "SMITH",
	"Address" : "1913 Hanoi Way",
	"District" : "Nagasaki",
	"City" : "Sasebo",
	"Country" : "Japan",
	"Phone" : "28303384290",
	"Rentals" : [
		{
			"rentalId" : 1185,
			"Rental Date" : "2005-06-15 00:54:12.0",
			"Return Date" : "2005-06-23 02:42:12.0",
			"staffId" : 2,
			"filmId" : 611,
			"Film Title" : "MUSKETEERS WAIT",
			"Payments" : [
				{
					"Payment Id" : 3,
					"Amount" : 6.00,
					"Payment Date" : "2005-06-15 00:54:12.0"
				}
			]
		},
		{
			"rentalId" : 1476,
			"Rental Date" : "2005-06-15 21:08:46.0",
			"Return Date" : "2005-06-25 02:26:46.0",
			"staffId" : 1,
			"filmId" : 308,
			"Film Title" : "FERRIS MOTHER",
			"Payments" : [
				{
					"Payment Id" : 5,
					"Amount" : 10.00,
					"Payment Date" : "2005-06-15 21:08:46.0"
				}
			]
		},
		{
			"rentalId" : 1725,
			"Rental Date" : "2005-06-16 15:18:57.0",
			"Return Date" : "2005-06-17 21:05:57.0",
			"staffId" : 1,
			"filmId" : 159,
			"Film Title" : "CLOSER BANG",
			"Payments" : [
				{
					"Payment Id" : 6,
					"Amount" : 5.00,
					"Payment Date" : "2005-06-16 15:18:57.0"
				}
			]
		}
  ]
}
```

### Inserció

#### Exercici 1

Habitualment utilitzarem la utilitat *mongoimport* per importar un fitxer
JSON a MongoDB.

Aquest cop, per tal de practicar la inserció de documents, farem un programa
en Java que ens creï una col·lecció i ens l'ompli amb les seves dades a partir
d'un fitxer JSON.

El [document](exercicis/zips.json) que volem importar s'ha extret de
http://media.mongodb.org/zips.json i representa els codis postals d'EEUU.

Per a cada codi postal, tenim la població al qual pertany, la seva
localització geogràfica, la població de la zona i l'estat al qual pertany.

De cara als propers exercicis és important notar que una mateixa població
pot tenir diversos codis postals.

Aprofitant que cada document que volem importar ocupa una sola línia al
fitxer JSON original, fes un programa en Java que llegeixi el fitxer
línia a línia i que importi les seves dades a una col·lecció de MongoDB
anomenada *zips*.

Quan el programa acabi volem que mostri el total de registres importats i
el temps que s'ha tardat a fer la importació.

### Consultes

En els següents exercicis utilitzarem la col·lecció de codis postals que hem
importat abans.

Tots ells s'han de resoldre sense utilitzar l'*aggregation framework*.

#### Exercici 1

Fes un programa que demani a l'usuari el nom d'una població d'EEUU i que
mostri tots els codis postals que li corresponen.

#### Exercici 2

Fes un programa que mostri els 10 codis postals amb més població.

#### Exercici 3

Fes un programa que mostri a quina població pertany i a quines coordenades
geogràfiques està situat el codi postal que està just a la mitad de totes les
poblacions pel que fa a la seva latitud.

Amb això ens referim al codi postal que té el mateix nombre de codis postals
més al nord que més al sud.

#### Exercici 4

Fes un programa que mostri el nom de totes les poblacions que tenen menys de
50 habitants.

Pots suposar que aquestes poblacions tindran sempre un sol codi postal.

#### Exercici 5

Fes un programa que mostri, per a cada estat, quin és el seu codi postal més
baix i quin el més alt.

Per a aquests codis postals volem saber també a quina població pertanyen.

#### Exercici 6

Volem saber tots els codis postals corresponents a Kansas City, així com la
població associada a cadascun.

Finalment, volem saber la població total de Kansas City.

#### Exercici 7

Sabent que a cap estat d'EEUU hi ha més d'una població amb el nom de
Springfield, fes un programa que mostri quantes poblacions de nom Springfield
diferents hi ha a EEUU.

De cadascuna d'aquestes poblacions volem saber a quin estat es troben i la
seva localització.

En cas que una mateixa població tingui diversos codis postals, considerarem
la seva localització geogràfica com la mitjana de les localitzacions de
cadascun dels seus codis postals.

### Modificació i eliminació de dades

#### Exercici 1

Fes un programa que demani a l'usuari un codi postal i que mostri les dades
del codi postal seleccionat.

Posteriorment, l'usuari podrà modificar la població d'aquest codi postal.

#### Exercici 2

Refés el programa de l'exercici 1 afegint-hi una interfície gràfica.

### *Aggregation framework*

Importa les col·leccions [students](exercicis/students.json) i
[grades](exercicis/grades.json). Els següents exercicis treballen sobre
aquestes col·leccions.

#### Exercici 1

Fes un programa en Java que, a partir de la col·lecció *students* ens retorni
una llista amb tots els alumnes que han tret un 40 o més de la mitjana de tots
els seus exàmens.

#### Exercici 2

Volem saber quantes activitats de cada tipus ha realitzat cada alumne de la
col·lecció *students*. Fes un programa que ens mostri aquesta informació
per a cada estudiant, ordenant-los pel seu nom.

#### Exercici 3

Fes un programa que mostri la mitjana de notes de cadascun dels tipus
d'activitat que hi ha.

#### Exercici 4

Per a cada alumnes de la col·lecció *students* volem calcular la seva nota
final utilitzant un programa en Java.

La nota es calcula de la següent manera:

60% de la mitjana de les notes dels exàmens, un 30% de la mitjana de la nota
dels deures i un 10% de la mitjana de la nota dels testos.

Un estudiant ha d'haver tret almenys un 40 de mitjana als exàmens per superar
el curs, en cas contrari tindrà una nota màxima de 40.

Fes un programa que mostri la nota final de cada estudiant.

#### Exercici 5

Fes un programa que demani el nom d'un estudiant i mostri la seva nota final,
així com les notes i tipus de cadascuna de les activitats que ha fet.

#### Exercici 6

Per a la col·lecció *grades* volem saber la mitjana de la nota dels exàmens
per a cadascuna de les classes que hi ha. Quina és la classe que ha tret,
de mitjana, una millor nota als exàmens?

#### Exercici 7

A la col·lecció *grades* un mateix estudiant pot estar a diverses classes.
Volem fer un programa que, donat el codi d'un estudiant, ens mostri el
tipus d'activitat i nota de tot el que ha fet, independentment de a quina
classe hagi obtingut les notes.

#### Exercici 8

Volem fer un programa que ens mostri, a partir de la col·lecció *grades*, una
llista amb totes les classes que hi ha i quins alumnes hi ha a cada classe.

### Convocatòria extraordinària

#### Exercici 1

Fes un programa que permeti gestionar la col·lecció *students*. El programa
ha de permetre:

- Afegir notes a un alumne. Primer se selecciona l'alumne pel nom, i a
continuació es permet afegir activats de tipus *exam*, *quiz* o *homework* amb
la seva nota.
- Esborrar notes d'un alumne. Se selecciona l'alumne per nom, es mostren totes
les seves notes, i es permet esborrar la nota que es vulgui.
- Calcular la mitjana de notes d'un alumne. Se selecciona l'alumne per nom i
es mostra un informe amb: totes les seves notes, la mitjana de les seves notes
per a cada tipus d'activitat, i la seva nota final (que es calcula com
s'explica a l'exercici 4 d'*aggregation framework*).
